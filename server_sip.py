#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys


class SIPRequest():

    def __init__(self, data):
        self.data = data.decode('utf-8')

    def parse(self):
        self._parse_command(self.data)
        headersLn = self.data.split('\n')[1:]
        self._parse_headers(headersLn[0])

    def _get_address(self, uri):
        addressSchema = uri.split(':')
        address = addressSchema [1]
        Schema = addressSchema[0]
        return address, Schema

    def _parse_command(self, line):
        UriCommand = line.split(" ")
        self.command = UriCommand[0]
        self.uri = UriCommand[1]
        self.address, Schema = self._get_address(self.uri)

        if self.command != "REGISTER":
            self.result = "405 Method Not Allowed"
        elif schema != "sip":
            self.result = "416 Unsupported URI Scheme"
        else:
            self.result = "200 OK"

    def _parse_headers(self, first_nl):
        self.headers = {}
        for head in first_nl:
            if len(head) > 0:
                hd = head.split(":")
                hd2 = hd[0]
                val = hd[1].replace(" ", " ")
                self.headers[hd2] = val


class sipRegisterHandler(socketserver.BaseRequestHandler):
    IP = {}

    def process_register(self, request):
        if request.headers["Expires"] == "0" and self.IP != {}:
            self.IP.pop(request.address)
        elif request.headers["Expires"] == "0":
            self.IP = self.IP
        else:
            self.IP[request.address] = f"{self.client_address[0]}"

            print(f" Se han registrado los siguientes usuarios: {self.IP}")


    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]
        sipRequest = SIPRequest(data)
        sipRequest.parse()
        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)
        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)



def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), EchoHandler)
        print(f"Lanzando servidor UDP de eco ({PORT})...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
