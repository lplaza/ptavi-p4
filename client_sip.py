#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socket
import sys

# Constantes. Contenido que se envía y dirección IP
PORT = int(sys.argv[2])
MTH = sys.argv[3]
MSG = sys.argv[4]
HEAD = sys.argv[5]
SERVER = sys.argv[1]

def main():
    if len(sys.argv) < 4:
        sys.exit("Se necesitan al menos 4 argumentos")
    # Creo el socket, se configura y se añade a un servidor
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            request = MTH.upper() + "sip:" + MSG + "SIP/2.0\r\n" + "Expires:" + HEAD + "\r\n\r\n"
            my_socket.sendtop(request.encode('utf-8'), (SERVER, PORT))
            data = my_socket.recv(1024)
            print(data.decode("utf-8"))
    except ConnectionRefusedError:
        print("Error al conectarse al servidor")

if __name__ == "__main__":
    main()
